package com.example.hexarch.adapters.out.persistence;

import com.example.hexarch.users.adapters.out.persistence.GetUserPersistenceAdapter;
import com.example.hexarch.users.adapters.out.persistence.UpdateUserPersistenceAdapter;
import com.example.hexarch.users.domain.User;
import com.example.hexarch.users.domain.ex.InvalidUserDataException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import({GetUserPersistenceAdapter.class, UpdateUserPersistenceAdapter.class})
public class UpdateUserPersistenceAdapterTest {

    @Autowired
    private GetUserPersistenceAdapter getUserPersistenceAdapter;

    @Autowired
    private UpdateUserPersistenceAdapter updateUserPersistenceAdapter;

    @Autowired
    private EntityManager entityManager;


    @Test
    public void registryNewUserTest () throws InvalidUserDataException {

        //Given
        List<User> initialUsers = new ArrayList<>(getUserPersistenceAdapter.getAllUsers());
        assertThat(initialUsers).isEmpty();

        User registerUser = User.builder()
                .name("NewUser")
                .surname("NewUser")
                .age(90)
                .build();

        //When
        updateUserPersistenceAdapter.registerUser(registerUser);

        //Then
        List<User> allUsers = new ArrayList<>(getUserPersistenceAdapter.getAllUsers());

        assertThat(allUsers.size()).isEqualTo(1);
        assertThat(allUsers.get(0).getName()).isEqualToIgnoringCase("NewUser");
    }

    @Test
    @Sql("importUsers.sql")
    public void updateUserTest () {
        //Given
        List<User> initialUsers = new ArrayList<>(getUserPersistenceAdapter.getAllUsers());
        assertThat(initialUsers.size()).isEqualTo(4);

        Long userIdToUpdate = initialUsers.get(0).getId();
        String newName = "Changed";
        User updatedUser = User.builder()
                .name(newName)
                .build();

        //When
        updateUserPersistenceAdapter.updateUser(userIdToUpdate.toString(), updatedUser);

        //Then
        Optional<User> savedUser = getUserPersistenceAdapter.getUser(userIdToUpdate.toString());

        assertThat(savedUser).isNotEmpty();
        assertThat(savedUser.get().getId()).isEqualTo(userIdToUpdate);
        assertThat(savedUser.get().getName()).isEqualTo(newName);
        assertThat(savedUser.get().getAge()).isNotNull();
    }
}
