package com.example.hexarch.adapters.out.persistence;

import com.example.hexarch.users.adapters.out.persistence.DeleteUserPersistenceAdapter;
import com.example.hexarch.users.adapters.out.persistence.GetUserPersistenceAdapter;
import com.example.hexarch.users.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import({GetUserPersistenceAdapter.class, DeleteUserPersistenceAdapter.class})
public class DeleteUserPersistenceAdapterTest {

    @Autowired
    private GetUserPersistenceAdapter getUserPersistenceAdapter;

    @Autowired
    private DeleteUserPersistenceAdapter deleteUserPersistenceAdapter;

    @Test
    @Sql("importUsers.sql")
    public void deleteOneUserTest () {

        //Given
        List<User> allUsers = new ArrayList<>(getUserPersistenceAdapter.getAllUsers());

        assertThat(allUsers.size()).isEqualTo(4);

        User randomUser = allUsers.get(0);

        //When
        Optional<User> deletedUser = deleteUserPersistenceAdapter.deleteUser(randomUser.getId().toString());

        //Then
        assertThat(deletedUser).isNotEmpty();

        allUsers = new ArrayList<>(getUserPersistenceAdapter.getAllUsers());

        assertThat(allUsers.size()).isEqualTo(3);
    }

    @Test
    @Sql("importUsers.sql")
    public void deleteAllUsersTest () {
        //Given
        List<User> allUsers = new ArrayList<>(getUserPersistenceAdapter.getAllUsers());

        assertThat(allUsers.size()).isEqualTo(4);

        //When
        deleteUserPersistenceAdapter.deleteAllUsers();

        //Then
        allUsers = new ArrayList<>(getUserPersistenceAdapter.getAllUsers());

        assertThat(allUsers).isEmpty();
    }
}
