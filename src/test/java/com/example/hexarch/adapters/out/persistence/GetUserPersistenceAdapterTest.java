package com.example.hexarch.adapters.out.persistence;

import com.example.hexarch.users.adapters.out.persistence.GetUserPersistenceAdapter;
import com.example.hexarch.users.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import({GetUserPersistenceAdapter.class})
public class GetUserPersistenceAdapterTest {

    @Autowired
    private GetUserPersistenceAdapter getUserPersistenceAdapter;

    @Test
    @Sql("importUsers.sql")
    public void getAllUsersTest () {
        List<User> allUsers = new ArrayList<>(getUserPersistenceAdapter.getAllUsers());

        assertThat(allUsers.size()).isEqualTo(4);
    }

    @Test
    @Sql("importUsers.sql")
    public void getAnUserByIdTest () {
        List<User> allUsers = new ArrayList<>(getUserPersistenceAdapter.getAllUsers());

        assertThat(allUsers).isNotEmpty();

        User randomUser = allUsers.get(0);
        String userId = randomUser.getId().toString();
        Optional<User> userFound = getUserPersistenceAdapter.getUser(userId);

        assertThat(userFound).isNotEmpty();
        assertThat(userFound.get().getName()).isEqualToIgnoringCase(randomUser.getName());
    }
}
