package com.example.hexarch.adapters.in.web;

import com.example.hexarch.users.adapters.in.web.UserController;
import com.example.hexarch.users.adapters.in.web.model.RegisterUserRequest;
import com.example.hexarch.users.adapters.in.web.model.UserResponse;
import com.example.hexarch.users.application.DeleteUserService;
import com.example.hexarch.users.application.GetUserService;
import com.example.hexarch.users.application.UpdateUserService;
import com.example.hexarch.users.domain.User;
import com.example.hexarch.users.domain.ex.InvalidUserDataException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private GetUserService getUserService;

    @MockBean
    private UpdateUserService updateUserService;

    @MockBean
    private DeleteUserService deleteUserService;

    @Test
    public void anyUserNotFoundTest () throws Exception {

        //Given
        given(getUserService.getAllUsers()).willReturn(Collections.emptyList());

        //When / Then
        mockMvc
            .perform(
                get("/user")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            ).andExpect(status().isNotFound());
    }

    @Test
    public void noUserFoundTest () throws Exception {

        //Given
        given(getUserService.getUserById("anyId")).willReturn(Optional.empty());

        //When / Then
        mockMvc
                .perform(
                        get("/user/anyId")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andExpect(status().isNotFound());
    }

    @Test
    public void allUserTest () throws Exception {

        //Given
        User oneUser = User.builder()
                .id(1L)
                .name("test")
                .surname("test")
                .age(40)
                .build();
        List<User> usersFound = Collections.singletonList(oneUser);
        given(getUserService.getAllUsers()).willReturn(usersFound);

        //When
        MvcResult result = mockMvc
                .perform(
                        get("/user")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andReturn();

        //Then
        String jsonResult = result.getResponse().getContentAsString();

        ArrayList<UserResponse> response = objectMapper.readValue(jsonResult, new TypeReference<>(){});

        assertThat(response.size()).isEqualTo(1);
        assertThat(response.get(0).getId()).isEqualTo(1L);
        assertThat(response.get(0).getAge()).isEqualTo(40);
    }

    @Test
    public void userFoundTest () throws Exception {

        //Given
        User oneUser = User.builder()
                .id(1L)
                .name("test")
                .surname("test")
                .age(40)
                .build();
        given(getUserService.getUserById("1")).willReturn(Optional.of(oneUser));

        //When
        MvcResult result = mockMvc
                .perform(
                        get("/user/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andReturn();

        //Then
        String jsonResult = result.getResponse().getContentAsString();
        UserResponse userResponse = objectMapper.readValue(jsonResult, UserResponse.class);

        assertThat(userResponse.getAge()).isEqualTo(40);
        assertThat(userResponse.getName()).isEqualToIgnoringCase("test");
    }

    @Test
    public void registerUserTest () throws Exception {

        //Given
        RegisterUserRequest request = new RegisterUserRequest();
        request.setName("test");
        request.setSurname("test");
        request.setAge(35);

        String jsonRequest = objectMapper.writeValueAsString(request);

        //When
        mockMvc.perform(
                post("/user")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonRequest)
                ).andExpect(status().isCreated());
    }

    @Test
    public void registerUserFailedTest () throws Exception {

        //Given
        RegisterUserRequest request = new RegisterUserRequest();
        request.setName("test");
        request.setSurname("test");
        request.setAge(35);

        willThrow(new InvalidUserDataException()).given(updateUserService).registerUser(any(User.class));

        String jsonRequest = objectMapper.writeValueAsString(request);

        //When
        mockMvc.perform(
                post("/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest)
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void updateUserTest () throws Exception {
        //Given
        RegisterUserRequest request = new RegisterUserRequest();
        request.setName("test");

        User modifiedUser = User.builder()
                .id(2L)
                .name("test")
                .surname("test")
                .age(20)
                .build();

        given(updateUserService.updateUser(any(String.class), any(User.class))).willReturn(Optional.of(modifiedUser));

        String jsonRequest = objectMapper.writeValueAsString(request);

        //When
        MvcResult result = mockMvc.perform(
                    put("/user/2")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(jsonRequest)
                )
                .andExpect(status().isAccepted())
                .andReturn();

        String jsonResult = result.getResponse().getContentAsString();

        UserResponse userResponse = objectMapper.readValue(jsonResult, UserResponse.class);

        assertThat(userResponse.getName()).isEqualToIgnoringCase("test");
        assertThat(userResponse.getAge()).isEqualTo(20);
    }

    @Test
    public void updateUserFailTest () throws Exception {
        //Given
        RegisterUserRequest request = new RegisterUserRequest();
        request.setName("Test");

        given(updateUserService.updateUser(any(String.class), any(User.class)))
                .willReturn(Optional.empty());

        //When
        mockMvc.perform(
                    put("/user/1")
                        .content(objectMapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isNotFound());
    }

    @Test
    public void deleteAllUsers () throws Exception {
        mockMvc.perform(
                delete("/user")
        ).andExpect(status().isOk());
    }

    @Test
    public void deleteOneUser () throws Exception {
        //Given
        String userId = "1";
        User deletedUser = User.builder()
                .id(Long.parseLong(userId))
                .name("Test")
                .surname("Test")
                .age(30)
                .build();

        given(deleteUserService.deleteUserById("1")).willReturn(Optional.of(deletedUser));

        //When
        MvcResult result = mockMvc.perform(
                delete("/user/1")
                    .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        //Then
        UserResponse deletedUserResponse = objectMapper.readValue(result.getResponse().getContentAsString(), UserResponse.class);
        assertThat(deletedUserResponse.getId()).isEqualTo(Long.parseLong(userId));
        assertThat(deletedUserResponse.getAge()).isEqualTo(30);
    }

    @Test
    public void deleteOneUserFails () throws Exception {
        //Given
        String userId = "1";

        given(deleteUserService.deleteUserById(userId)).willReturn(Optional.empty());

        //When
        mockMvc.perform(
                delete("/user/1")
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNotFound());
    }
}
