package com.example.hexarch.application;

import com.example.hexarch.users.application.GetUserService;
import com.example.hexarch.users.application.ports.out.GetUserPort;
import com.example.hexarch.users.domain.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class GetUserServiceTest {

    @Test
    public void testGetUser () {

        //Given
        String emptyUser = "";
        String anUser = "123";
        User testUser = User.builder()
                .id(Long.parseLong(anUser))
                .name("Test")
                .surname("surTest")
                .age(50)
                .build();

        GetUserPort getUserPort = Mockito.mock(GetUserPort.class);
        when(getUserPort.getUser(emptyUser)).thenReturn(Optional.empty());
        when(getUserPort.getUser(anUser)).thenReturn(Optional.of(testUser));

        GetUserService getUserService = new GetUserService(getUserPort);

        //When
        Optional<User> empty = getUserService.getUserById(emptyUser);
        Optional<User> user = getUserService.getUserById(anUser);

        //Then
        assertThat(empty).isEmpty();
        assertThat(user).isNotEmpty();
    }

    @Test
    public void testGetAllUsersEmpty () {

        //Given
        GetUserPort getUserPortNoUsers = Mockito.mock(GetUserPort.class);
        when(getUserPortNoUsers.getAllUsers()).thenReturn(Collections.emptyList());

        GetUserService getUserService = new GetUserService(getUserPortNoUsers);

        //When
        Collection<User> empty = getUserService.getAllUsers();

        //Then
        assertThat(empty).isEmpty();
    }

    @Test
    public void testGetAllUsers () {

        //Given
        GetUserPort getUserPortSomeUsers = Mockito.mock(GetUserPort.class);
        User user = User.builder()
                .id(1L)
                .name("Test")
                .surname("Test")
                .age(50)
                .build();
        when(getUserPortSomeUsers.getAllUsers()).thenReturn(List.of(user));

        GetUserService getUserService = new GetUserService(getUserPortSomeUsers);

        //When
        List<User> someUsers = new ArrayList<>(getUserService.getAllUsers());

        //Then
        assertThat(someUsers).isNotEmpty();
        assertThat(someUsers.get(0).getName()).isEqualTo("Test");
    }
}
