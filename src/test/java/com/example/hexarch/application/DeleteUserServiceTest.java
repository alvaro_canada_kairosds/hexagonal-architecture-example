package com.example.hexarch.application;

import com.example.hexarch.users.application.DeleteUserService;
import com.example.hexarch.users.application.ports.out.DeleteUserPort;
import com.example.hexarch.users.domain.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class DeleteUserServiceTest {

    @Test
    public void deleteUser () {

        //Given
        String noUserId = "";
        String someUserId = "123";
        User someUser = User.builder()
                .id(Long.parseLong(someUserId))
                .name("Test")
                .surname("Test")
                .age(50)
                .build();

        DeleteUserPort deleteUserPort = Mockito.mock(DeleteUserPort.class);
        when(deleteUserPort.deleteUser(noUserId)).thenReturn(Optional.empty());
        when(deleteUserPort.deleteUser(someUserId)).thenReturn(Optional.of(someUser));

        DeleteUserService deleteUserService = new DeleteUserService(deleteUserPort);

        //When
        Optional<User> noUser = deleteUserService.deleteUserById(noUserId);
        Optional<User> user = deleteUserService.deleteUserById(someUserId);

        //Then
        assertThat(noUser).isEmpty();
        assertThat(user).isNotEmpty();
    }
}
