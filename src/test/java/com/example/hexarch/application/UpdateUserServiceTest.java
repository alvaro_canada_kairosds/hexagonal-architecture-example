package com.example.hexarch.application;

import com.example.hexarch.users.application.UpdateUserService;
import com.example.hexarch.users.application.ports.out.UpdateUserPort;
import com.example.hexarch.users.domain.User;
import com.example.hexarch.users.domain.ex.InvalidUserDataException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

public class UpdateUserServiceTest {

    @Test
    public void registerNewUserWithError () throws InvalidUserDataException {

        //Given
        User registerUser = User.builder()
                .name("Test")
                .surname("Test")
                .age(50)
                .build();
        UpdateUserPort updateUserPort = Mockito.mock(UpdateUserPort.class);
        doThrow(new InvalidUserDataException()).when(updateUserPort).registerUser(registerUser);

        UpdateUserService updateUserService = new UpdateUserService(updateUserPort);

        //When-Then
        assertThatThrownBy(() -> {
            updateUserService.registerUser(registerUser);
        }).isInstanceOf(InvalidUserDataException.class);
    }

    @Test
    public void updateNotFoundUser () {
        //Given
        String userId = "123";
        User updateUser = User.builder()
                .name("Test")
                .surname("Test")
                .age(50)
                .build();
        UpdateUserPort updateUserPort = Mockito.mock(UpdateUserPort.class);
        when(updateUserPort.updateUser(userId, updateUser)).thenReturn(Optional.empty());

        UpdateUserService updateUserService = new UpdateUserService(updateUserPort);

        //When
        Optional<User> userUpdated = updateUserService.updateUser(userId, updateUser);

        //Then
        assertThat(userUpdated).isEmpty();
    }
}
