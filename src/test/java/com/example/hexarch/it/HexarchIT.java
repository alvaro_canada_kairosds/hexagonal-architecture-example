package com.example.hexarch.it;

import com.example.hexarch.HexarchApplication;
import com.example.hexarch.users.adapters.in.web.model.RegisterUserRequest;
import com.example.hexarch.users.adapters.in.web.model.UpdateUserRequest;
import com.example.hexarch.users.adapters.in.web.model.UserResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = HexarchApplication.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class HexarchIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void getAllUsersFromEmptyApp () throws Exception {
        mockMvc.perform(
                get("/user")
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNotFound());
    }


    @Test
    public void getAllUsersFromApp () throws Exception {
        //Given
        Integer[] userIds = new Integer[]{1, 2, 3, 4, 5};
        Arrays.stream(userIds).sequential().forEach(this::registerTestUserById);

        //When
        MvcResult result = mockMvc.perform(
                get("/user")
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
        .andReturn();

        //Then
        List<UserResponse> usersList = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>(){});

        assertThat(usersList.size()).isEqualTo(userIds.length);
    }

    @Test
    public void getOneUserFromApp () throws Exception {
        //Given
        List<UserResponse> users = registerAndGetAllUsersRegistered();
        UserResponse randomUser = users.get(0);

        //When
        MvcResult result = mockMvc.perform(
                get("/user/"+randomUser.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
        .andReturn();

        //Then
        UserResponse userFromResponse = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>(){});

        assertThat(userFromResponse.getId()).isEqualTo(randomUser.getId());
        assertThat(userFromResponse.getAge()).isEqualTo(randomUser.getAge());
    }

    @Test
    public void updateOneUserDataFromApp () throws Exception {
        //Given
        List<UserResponse> users = registerAndGetAllUsersRegistered();
        UserResponse randomUser = users.get(0);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setAge(500);
        updateUserRequest.setSurname("TestSurname");

        String updateUserAsJson = objectMapper.writeValueAsString(updateUserRequest);

        //When
        MvcResult result = mockMvc.perform(
                put("/user/"+randomUser.getId())
                .content(updateUserAsJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isAccepted())
        .andReturn();

        //Then
        UserResponse updatedUserResponse = objectMapper.readValue(result.getResponse().getContentAsString(), UserResponse.class);

        assertThat(updatedUserResponse.getId()).isEqualTo(randomUser.getId());
        assertThat(updatedUserResponse.getName()).isEqualTo(randomUser.getName());
        assertThat(updatedUserResponse.getAge()).isEqualTo(500);
        assertThat(updatedUserResponse.getAge()).isNotEqualTo(randomUser.getAge());
        assertThat(updatedUserResponse.getSurname()).isEqualTo("TestSurname");
        assertThat(updatedUserResponse.getSurname()).isNotEqualTo(randomUser.getSurname());
    }

    private List<UserResponse> registerAndGetAllUsersRegistered () throws Exception {
        Integer[] userIds = new Integer[]{1, 2, 3, 4, 5};
        Arrays.stream(userIds).sequential().forEach(this::registerTestUserById);

        MvcResult result = mockMvc.perform(
                get("/user")
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andReturn();

        return objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});
    }

    private void registerTestUserById (Integer id) {
        try {
            RegisterUserRequest userRequest = new RegisterUserRequest();
            userRequest.setName("Test"+id);
            userRequest.setSurname("Test"+id);
            userRequest.setAge(id * 5);

            String userRequestJson = objectMapper.writeValueAsString(userRequest);

            mockMvc.perform(
                    post("/user")
                        .content(userRequestJson)
                        .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(status().isCreated());
        } catch (Exception e) {
            fail("Test fails loading users on database through api");
        }
    }
}
