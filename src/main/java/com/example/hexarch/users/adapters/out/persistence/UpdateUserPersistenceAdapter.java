package com.example.hexarch.users.adapters.out.persistence;

import com.example.hexarch.users.adapters.out.persistence.dao.UserRepository;
import com.example.hexarch.users.adapters.out.persistence.entity.UserJpaEntity;
import com.example.hexarch.users.adapters.out.persistence.mapping.JpaUserMapping;
import com.example.hexarch.users.application.ports.out.UpdateUserPort;
import com.example.hexarch.users.domain.User;
import com.example.hexarch.users.domain.ex.InvalidUserDataException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UpdateUserPersistenceAdapter implements UpdateUserPort {

    private final UserRepository userRepository;

    public UpdateUserPersistenceAdapter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void registerUser(User user) throws InvalidUserDataException {
        UserJpaEntity userJpaEntity = JpaUserMapping.getJpaUserFromUser(user);
        try {
            userRepository.save(userJpaEntity);
        } catch (Exception e) {
            throw new InvalidUserDataException("Something happends saving new user", e);
        }
    }

    @Override
    public Optional<User> updateUser(String id, User user) {
        Optional<User> optFromNewUser = Optional.of(user);
        Optional<UserJpaEntity> optFromActualUser = userRepository.findById(Long.parseLong(id));

        if (optFromActualUser.isEmpty()) {
            return Optional.empty();
        }

        UserJpaEntity actualUser = optFromActualUser.get();
        UserJpaEntity updatedUser = UserJpaEntity.builder()
                .id(actualUser.getId())
                .name(optFromNewUser.map(User::getName).orElse(actualUser.getName()) )
                .surname(optFromNewUser.map(User::getSurname).orElse(actualUser.getSurname()))
                .age(optFromNewUser.map(User::getAge).orElse(actualUser.getAge()))
                .build();

        UserJpaEntity savedUser = userRepository.save(updatedUser);

        return Optional.of(JpaUserMapping.getUserFromJpaUser(savedUser));
    }
}
