package com.example.hexarch.users.adapters.in.web.model;

import lombok.Data;

@Data
public class RegisterUserRequest {

    private String name;

    private String surname;

    private Integer age;
}
