package com.example.hexarch.users.adapters.in.web;

import com.example.hexarch.users.adapters.in.web.mapping.WebUserMapping;
import com.example.hexarch.users.adapters.in.web.model.RegisterUserRequest;
import com.example.hexarch.users.adapters.in.web.model.UpdateUserRequest;
import com.example.hexarch.users.adapters.in.web.model.UserResponse;
import com.example.hexarch.users.application.ports.in.DeleteUserUseCase;
import com.example.hexarch.users.application.ports.in.GetUserUseCase;
import com.example.hexarch.users.application.ports.in.UpdateUserUseCase;
import com.example.hexarch.users.domain.User;
import com.example.hexarch.users.domain.ex.InvalidUserDataException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private GetUserUseCase getUserUseCase;
    private UpdateUserUseCase updateUserUseCase;
    private DeleteUserUseCase deleteUserUseCase;

    public UserController (GetUserUseCase getUserUseCase, UpdateUserUseCase updateUserUseCase, DeleteUserUseCase deleteUserUseCase) {
        this.getUserUseCase = getUserUseCase;
        this.updateUserUseCase = updateUserUseCase;
        this.deleteUserUseCase = deleteUserUseCase;
    }

    /**
     * Get all users on the app
     */
    @RequestMapping(method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<UserResponse>> getAllUsers () {
        Collection<User> allUsers = getUserUseCase.getAllUsers();

        return allUsers.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(
                    allUsers.parallelStream().map(WebUserMapping::userToUserResponse).collect(Collectors.toList()),
                    HttpStatus.OK);
    }

    /**
     * Get an user by id
     */
    @RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> getUser (@PathVariable("id") String userId) {
        Optional<User> optUser = getUserUseCase.getUserById(userId);

        return optUser.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(optUser.map(WebUserMapping::userToUserResponse).get(), HttpStatus.OK);
    }

    /**
     * Register a new user
     *
     * @param registerUserRequest new user request info
     */
    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> registerUser (@RequestBody RegisterUserRequest registerUserRequest) {
        User registerUser = WebUserMapping.newUserRequestToUser(registerUserRequest);
        try {
            updateUserUseCase.registerUser(registerUser);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (InvalidUserDataException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Modify an existing user info
     *
     * @param updateUserRequest update user request info
     */
    @RequestMapping(value = "/{id}", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> updateUser (@PathVariable("id") String id, @RequestBody UpdateUserRequest updateUserRequest) {
        User userToUpdate = WebUserMapping.updateUserRequestToUser(updateUserRequest);
        Optional<User> updatedUser = updateUserUseCase.updateUser(id, userToUpdate);

        return updatedUser.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(updatedUser.map(WebUserMapping::userToUserResponse).get(), HttpStatus.ACCEPTED);

    }

    /**
     * Delete all users
     */
    @RequestMapping(method = DELETE)
    public ResponseEntity<?> deleteAllUsers () {
        deleteUserUseCase.deleteAllUsers();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Delete a user by id
     */
    @RequestMapping(value = "/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteUser (@PathVariable("id") String id) {
        Optional<User> optUserDeleted = deleteUserUseCase.deleteUserById(id);

        return optUserDeleted.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(optUserDeleted.map(WebUserMapping::userToUserResponse).get(), HttpStatus.OK);
    }
}
