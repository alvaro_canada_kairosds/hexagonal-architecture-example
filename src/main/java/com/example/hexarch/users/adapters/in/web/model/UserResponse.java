package com.example.hexarch.users.adapters.in.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    private long id;

    private String name;

    private String surname;

    private int age;
}
