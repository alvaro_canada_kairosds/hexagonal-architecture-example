package com.example.hexarch.users.adapters.in.web.mapping;

import com.example.hexarch.users.adapters.in.web.model.RegisterUserRequest;
import com.example.hexarch.users.adapters.in.web.model.UpdateUserRequest;
import com.example.hexarch.users.adapters.in.web.model.UserResponse;
import com.example.hexarch.users.domain.User;

import java.util.Collection;
import java.util.stream.Collectors;

public class WebUserMapping {

    public static User newUserRequestToUser(RegisterUserRequest registerUserRequest) {
        return User.builder()
                .name(registerUserRequest.getName())
                .surname(registerUserRequest.getSurname())
                .age(registerUserRequest.getAge())
                .build();
    }

    public static User updateUserRequestToUser(UpdateUserRequest updateUserRequest) {
        return User.builder()
                .name(updateUserRequest.getName())
                .surname(updateUserRequest.getSurname())
                .age(updateUserRequest.getAge())
                .build();
    }

    public static UserResponse userToUserResponse(User user) {
        return UserResponse.builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .age(user.getAge())
                .build();
    }

    public static Collection<UserResponse> userCollectionToUserResponseCollection (Collection<User> users) {
        return users.stream()
                .map(WebUserMapping::userToUserResponse)
                .collect(Collectors.toList());
    }
}
