package com.example.hexarch.users.adapters.out.persistence;

import com.example.hexarch.users.adapters.out.persistence.dao.UserRepository;
import com.example.hexarch.users.adapters.out.persistence.entity.UserJpaEntity;
import com.example.hexarch.users.adapters.out.persistence.mapping.JpaUserMapping;
import com.example.hexarch.users.application.ports.out.GetUserPort;
import com.example.hexarch.users.domain.User;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class GetUserPersistenceAdapter implements GetUserPort {

    private final UserRepository userRepository;

    public GetUserPersistenceAdapter (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> getUser(String id) {
        return userRepository
                .findById(Long.parseLong(id))
                .map(JpaUserMapping::getUserFromJpaUser);
    }

    @Override
    public Collection<User> getAllUsers() {
        Iterable<UserJpaEntity> entityUsers = userRepository.findAll();

        return StreamSupport
                .stream(entityUsers.spliterator(), true)
                .map(JpaUserMapping::getUserFromJpaUser)
                .collect(Collectors.toList());
    }
}
