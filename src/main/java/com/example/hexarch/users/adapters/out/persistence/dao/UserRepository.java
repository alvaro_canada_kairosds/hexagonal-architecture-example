package com.example.hexarch.users.adapters.out.persistence.dao;

import com.example.hexarch.users.adapters.out.persistence.entity.UserJpaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserJpaEntity, Long> {}
