package com.example.hexarch.users.adapters.out.persistence.mapping;

import com.example.hexarch.users.adapters.out.persistence.entity.UserJpaEntity;
import com.example.hexarch.users.domain.User;

public class JpaUserMapping {

    public static User getUserFromJpaUser (UserJpaEntity jpaUser) {
        return User.builder()
                .id(jpaUser.getId())
                .name(jpaUser.getName())
                .surname(jpaUser.getSurname())
                .age(jpaUser.getAge())
                .build();
    }

    public static UserJpaEntity getJpaUserFromUser (User user) {
        return UserJpaEntity.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .age(user.getAge())
                .build();
    }
}
