package com.example.hexarch.users.adapters.out.persistence;

import com.example.hexarch.users.adapters.out.persistence.dao.UserRepository;
import com.example.hexarch.users.adapters.out.persistence.entity.UserJpaEntity;
import com.example.hexarch.users.adapters.out.persistence.mapping.JpaUserMapping;
import com.example.hexarch.users.application.ports.out.DeleteUserPort;
import com.example.hexarch.users.domain.User;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DeleteUserPersistenceAdapter implements DeleteUserPort {

    private final UserRepository userRepository;

    public DeleteUserPersistenceAdapter (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> deleteUser(String id) {
        Optional<UserJpaEntity> optUser = userRepository.findById(Long.parseLong(id));

        if (optUser.isEmpty()) {
            return Optional.empty();
        }

        UserJpaEntity userJpaEntity = optUser.get();

        userRepository.delete(userJpaEntity);

        return Optional.of(JpaUserMapping.getUserFromJpaUser(userJpaEntity));
    }

    @Override
    public void deleteAllUsers() {
        userRepository.deleteAll();
    }
}
