package com.example.hexarch.users.domain.ex;

public class InvalidUserDataException extends Exception{

    public InvalidUserDataException () {}

    public InvalidUserDataException (String message) {
        super(message);
    }

    public InvalidUserDataException (String message, Exception e) {
        super(message, e);
    }
}
