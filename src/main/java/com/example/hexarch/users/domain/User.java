package com.example.hexarch.users.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class User {

    private Long id;

    private String name;

    private String surname;

    private Integer age;
}
