package com.example.hexarch.users.application.ports.in;

import com.example.hexarch.users.domain.User;

import java.util.Optional;

public interface DeleteUserUseCase {

    Optional<User> deleteUserById (String id);

    void deleteAllUsers ();
}
