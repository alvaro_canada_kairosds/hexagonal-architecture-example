package com.example.hexarch.users.application;

import com.example.hexarch.users.application.ports.in.UpdateUserUseCase;
import com.example.hexarch.users.application.ports.out.UpdateUserPort;
import com.example.hexarch.users.domain.User;
import com.example.hexarch.users.domain.ex.InvalidUserDataException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UpdateUserService implements UpdateUserUseCase {

    private final UpdateUserPort updateUserPort;

    public UpdateUserService (UpdateUserPort updateUserPort) {
        this.updateUserPort = updateUserPort;
    }


    @Override
    public void registerUser(User user) throws InvalidUserDataException {
        updateUserPort.registerUser(user);
    }

    @Override
    public Optional<User> updateUser(String id, User user) {
        return updateUserPort.updateUser(id, user);
    }
}
