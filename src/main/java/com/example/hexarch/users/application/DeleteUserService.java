package com.example.hexarch.users.application;

import com.example.hexarch.users.application.ports.in.DeleteUserUseCase;
import com.example.hexarch.users.application.ports.out.DeleteUserPort;
import com.example.hexarch.users.domain.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DeleteUserService implements DeleteUserUseCase {

    private final DeleteUserPort deleteUserPort;

    public DeleteUserService (DeleteUserPort deleteUserPort) {
        this.deleteUserPort = deleteUserPort;
    }

    @Override
    public Optional<User> deleteUserById(String id) {
        return deleteUserPort.deleteUser(id);
    }

    @Override
    public void deleteAllUsers() {
        deleteUserPort.deleteAllUsers();
    }
}
