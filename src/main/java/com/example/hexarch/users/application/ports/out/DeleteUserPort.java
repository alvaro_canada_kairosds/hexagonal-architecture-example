package com.example.hexarch.users.application.ports.out;

import com.example.hexarch.users.domain.User;

import java.util.Optional;

public interface DeleteUserPort {

    Optional<User> deleteUser (String id);

    void deleteAllUsers ();
}
