package com.example.hexarch.users.application.ports.out;

import com.example.hexarch.users.domain.User;

import java.util.Collection;
import java.util.Optional;

public interface GetUserPort {

    Optional<User> getUser (String id);

    Collection<User> getAllUsers ();
}
