package com.example.hexarch.users.application;

import com.example.hexarch.users.application.ports.in.GetUserUseCase;
import com.example.hexarch.users.application.ports.out.GetUserPort;
import com.example.hexarch.users.domain.User;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class GetUserService implements GetUserUseCase {

    private final GetUserPort getUserPort;

    public GetUserService(GetUserPort getUserPort) {
        this.getUserPort = getUserPort;
    }

    @Override
    public Optional<User> getUserById(String id) {
        return getUserPort.getUser(id);
    }

    @Override
    public Collection<User> getAllUsers() {
        return getUserPort.getAllUsers();
    }
}
