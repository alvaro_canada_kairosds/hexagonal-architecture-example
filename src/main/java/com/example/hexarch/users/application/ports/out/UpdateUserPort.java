package com.example.hexarch.users.application.ports.out;

import com.example.hexarch.users.domain.User;
import com.example.hexarch.users.domain.ex.InvalidUserDataException;

import java.util.Optional;

public interface UpdateUserPort {

    void registerUser (User user) throws InvalidUserDataException;

    Optional<User> updateUser(String id, User user);
}
