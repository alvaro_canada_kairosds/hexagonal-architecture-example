package com.example.hexarch.users.application.ports.in;

import com.example.hexarch.users.domain.User;
import com.example.hexarch.users.domain.ex.InvalidUserDataException;

import java.util.Optional;

public interface UpdateUserUseCase {

    void registerUser (User user) throws InvalidUserDataException;

    Optional<User> updateUser (String id, User user);
}
