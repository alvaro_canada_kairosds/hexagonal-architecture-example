package com.example.hexarch.users.application.ports.in;

import com.example.hexarch.users.domain.User;

import java.util.Collection;
import java.util.Optional;

public interface GetUserUseCase {

    Collection<User> getAllUsers ();

    Optional<User> getUserById (String id);
}
